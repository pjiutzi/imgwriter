"""
__init__
~~~~~~~~

The namespace of the imgwriter module.
"""
__all__ = ['imgwriter', 'imgreader']
from imgwriter.imgwriter import save
from imgwriter.imgreader import read_image
