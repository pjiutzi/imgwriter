"""
__version__
~~~~~~~~~~~

Common information about the imgwriter module, including the version
number.
"""

__title__ = 'imgwriter'
__description__ = 'A Python package for saving arrays as images or video.'
__version__ = '0.2.1'
__author__ = 'Paul J. Iutzi'
__license__ = 'MIT'
__copyright__ = 'Copyright (c) 2021 Paul J. Iutzi'
